
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <MeshObjectSelectionPlugin.hh>
#include <QString>
#include <QChar>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(MeshObjectSelection, m) {

  QObject* pluginPointer = getPluginPointer("MeshObjectSelection");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-MeshObjectSelection" << std::endl;
     return;
   }

  MeshObjectSelectionPlugin* plugin = qobject_cast<MeshObjectSelectionPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-MeshObjectSelection" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< MeshObjectSelectionPlugin,std::unique_ptr<MeshObjectSelectionPlugin, py::nodelete> > select(m, "MeshObjectSelection");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  select.def(py::init([plugin]() { return plugin; }));

  select.def("loadSelection", &MeshObjectSelectionPlugin::loadSelection,
                              QCoreApplication::translate("PythonDocMeshObjectSelection","Load selection from selection file").toLatin1().data(),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","File containing a selection").toLatin1().data()) );

  //==========================================
  // Vertex OPERATIONS
  //==========================================

  select.def("selectVertices", &MeshObjectSelectionPlugin::selectVertices,
                              QCoreApplication::translate("PythonDocMeshObjectSelection","Select the specified vertices").toLatin1().data(),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("unselectVertices", &MeshObjectSelectionPlugin::unselectVertices,
                              QCoreApplication::translate("PythonDocMeshObjectSelection","Unselect the specified vertices").toLatin1().data(),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("selectAllVertices", &MeshObjectSelectionPlugin::selectAllVertices,
                              QCoreApplication::translate("PythonDocMeshObjectSelection","Select all vertices of an object").toLatin1().data(),
                      py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("clearVertexSelection", &MeshObjectSelectionPlugin::clearVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear vertex selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("invertVertexSelection", &MeshObjectSelectionPlugin::invertVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Invert vertex selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectBoundaryVertices", &MeshObjectSelectionPlugin::selectBoundaryVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all boundary vertices of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectClosestBoundaryVertices", &MeshObjectSelectionPlugin::selectClosestBoundaryVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select boundary vertices closest to a specific vertex").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Id of closest vertex").toLatin1().data()) );

  select.def("shrinkVertexSelection", &MeshObjectSelectionPlugin::shrinkVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Shrink vertex selection by outer selection ring").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("growVertexSelection", &MeshObjectSelectionPlugin::growVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Grow vertex selection by an-ring of selection").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("deleteVertexSelection", &MeshObjectSelectionPlugin::deleteVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Delete selected vertices").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getVertexSelection", &MeshObjectSelectionPlugin::getVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Return a list of all selected vertices").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("createMeshFromVertexSelection", &MeshObjectSelectionPlugin::createMeshFromVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Create a mesh from the seslected vertices. Returns the Id of the new mesh").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("colorizeVertexSelection", &MeshObjectSelectionPlugin::colorizeVertexSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Colorize the selected vertices").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Red color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Green color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Blue color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Alpha color component").toLatin1().data()) );

  select.def("selectHandleVertices", &MeshObjectSelectionPlugin::selectHandleVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Add specified vertices to handle area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("unselectHandleVertices", &MeshObjectSelectionPlugin::selectHandleVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Remove specified vertices from handle area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("clearHandleVertices", &MeshObjectSelectionPlugin::clearHandleVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear handle area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("setAllHandleVertices", &MeshObjectSelectionPlugin::setAllHandleVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Add all vertices of an object to handle area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getHandleVertices", &MeshObjectSelectionPlugin::getHandleVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Get a list of vertices belonging to handle area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("loadFlipperModelingSelection", &MeshObjectSelectionPlugin::loadFlipperModelingSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Load selection from Flipper selection file").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Flipper selection file").toLatin1().data()) );

  select.def("saveFlipperModelingSelection", &MeshObjectSelectionPlugin::saveFlipperModelingSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Load selection from Flipper selection file").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Flipper selection file").toLatin1().data()) );

  select.def("selectModelingVertices", &MeshObjectSelectionPlugin::selectModelingVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Add specified vertices to modeling area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("unselectModelingVertices", &MeshObjectSelectionPlugin::unselectModelingVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Remove specified vertices to modeling area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()) );

  select.def("selectVerticesByValue", static_cast<void (MeshObjectSelectionPlugin::*)(int,QString,bool,double)> (&MeshObjectSelectionPlugin::selectVerticesByValue),
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select vertices based on the value of their component").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","component specification: string containing \"x\" or \"y\" or \"z\" ").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","select vertex if component greater than value; false: select if component less than value").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","value to test").toLatin1().data()) );

  select.def("clearModelingVertices", &MeshObjectSelectionPlugin::clearModelingVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear modeling area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("setAllModelingVertices", &MeshObjectSelectionPlugin::setAllModelingVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Add all vertices of an object to modeling area").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getModelingVertices", &MeshObjectSelectionPlugin::getModelingVertices,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Get a list of all modeling vertices").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  //==========================================
  // EDGE OPERATIONS
  //==========================================


  select.def("selectEdges", &MeshObjectSelectionPlugin::selectEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select the specified edges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of vertices").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Maximal dihedral ange for selection").toLatin1().data()) = 0.0);

  select.def("unselectEdges", &MeshObjectSelectionPlugin::unselectEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Unselect the specified edges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of edges").toLatin1().data()) );

  select.def("selectAllEdges", &MeshObjectSelectionPlugin::selectAllEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all edges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("invertEdgeSelection", &MeshObjectSelectionPlugin::invertEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Invert edge selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("clearEdgeSelection", &MeshObjectSelectionPlugin::clearEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear edge selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectBoundaryEdges", &MeshObjectSelectionPlugin::selectBoundaryEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all boundary edges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectBoundaryEdges", &MeshObjectSelectionPlugin::selectBoundaryEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all boundary edges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("deleteEdgeSelection", &MeshObjectSelectionPlugin::deleteEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Delete selected edges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getEdgeSelection", &MeshObjectSelectionPlugin::getEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Return a list of all selected edges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("convertEdgesToVertexPairs", &MeshObjectSelectionPlugin::convertEdgesToVertexPairs,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Convert edge ids to vertex pair ids. Returns vertex Idlist.").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of edges").toLatin1().data()));

  select.def("convertVertexPairsToEdges", &MeshObjectSelectionPlugin::convertVertexPairsToEdges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Convert vertex pair ids to edge ids. Returns edge Idlist.").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Ids of paired vertices").toLatin1().data()));

  select.def("createMeshFromEdgeSelection", &MeshObjectSelectionPlugin::createMeshFromEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Take edge selection of an object and create a new mesh from it").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("colorizeEdgeSelection", &MeshObjectSelectionPlugin::colorizeEdgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Colorize the selected edges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Red color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Green color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Blue color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Alpha color component").toLatin1().data()) );

  select.def("traceEdgePath", &MeshObjectSelectionPlugin::traceEdgePath,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Traces Edge Paths. Takes all selected edges and tries to continue them, as long as the angle between edges is not above threshold").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Threshold").toLatin1().data()));

  //==========================================
  // HALFEDGE OPERATIONS
  //==========================================

  select.def("selectHalfedges", &MeshObjectSelectionPlugin::selectHalfedges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select the specified halfedges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of halfedges").toLatin1().data()) );

  select.def("unselectHalfedges", &MeshObjectSelectionPlugin::unselectHalfedges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Unselect the specified halfedges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of halfedges").toLatin1().data()) );

  select.def("selectAllHalfedges", &MeshObjectSelectionPlugin::selectAllHalfedges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all halfedges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("invertHalfedgeSelection", &MeshObjectSelectionPlugin::invertHalfedgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Invert halfedge selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("clearHalfedgeSelection", &MeshObjectSelectionPlugin::clearHalfedgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear halfedge selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectBoundaryHalfedges", &MeshObjectSelectionPlugin::selectBoundaryHalfedges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all boundary halfedges of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getHalfedgeSelection", &MeshObjectSelectionPlugin::getHalfedgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Get a list of selected halfedges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("convertHalfedgesToVertexPairs", &MeshObjectSelectionPlugin::convertHalfedgesToVertexPairs,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Convert halfedge ids to vertex pair ids. Returns vertex Idlist.").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of halfedges").toLatin1().data()) );

  select.def("convertVertexPairsToHalfedges", &MeshObjectSelectionPlugin::convertVertexPairsToHalfedges,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Convert vertex pair ids to halfedge ids. Returns halfedge Idlist.").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Ids of paired vertices").toLatin1().data()) );

  select.def("colorizeHalfedgeSelection", &MeshObjectSelectionPlugin::colorizeHalfedgeSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Colorize the selected halfedges").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Red color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Green color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Blue color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Alpha color component").toLatin1().data()) );


  //==========================================
  // FACE OPERATIONS
  //==========================================

  select.def("selectFaces", &MeshObjectSelectionPlugin::selectFaces,
                                 QCoreApplication::translate("PythonDocMeshObjectSelection","Select the specified faces").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of faces").toLatin1().data()) );

  select.def("unselectFaces", &MeshObjectSelectionPlugin::unselectFaces,
                                 QCoreApplication::translate("PythonDocMeshObjectSelection","Unselect the specified faces").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","List of faces").toLatin1().data()) );

  select.def("selectAllFaces", &MeshObjectSelectionPlugin::selectAllFaces,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all faces of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("clearFaceSelection", &MeshObjectSelectionPlugin::clearFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Clear face selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("invertFaceSelection", &MeshObjectSelectionPlugin::invertFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Invert face selection of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("deleteFaceSelection", &MeshObjectSelectionPlugin::deleteFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Delete face that are currently selected").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("selectBoundaryFaces", &MeshObjectSelectionPlugin::selectBoundaryFaces,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Select all boundary faces of an object").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("shrinkFaceSelection", &MeshObjectSelectionPlugin::shrinkFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Shrink face selection by outer face ring of selection").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("growFaceSelection", &MeshObjectSelectionPlugin::growFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Grow face selection by an-ring of faces around selection").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("getFaceSelection", &MeshObjectSelectionPlugin::getFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Get current face selection").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("createMeshFromFaceSelection", &MeshObjectSelectionPlugin::createMeshFromFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Take face selection and create a new mesh from it").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()) );

  select.def("colorizeFaceSelection", &MeshObjectSelectionPlugin::colorizeFaceSelection,
                                QCoreApplication::translate("PythonDocMeshObjectSelection","Colorize the selected faces").toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Red color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Green color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Blue color component").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","Alpha color component").toLatin1().data()) );

  QString conversionStrings = QCoreApplication::translate("PythonDocMeshObjectSelection"," Possible strings:\n"
      "- Vertex/Edge/Halfedge/Face Selection\n"
      "- Model/Handle Region\n"
      "- Feature Vertices/Edges/Faces");


  select.def("convertSelection", &MeshObjectSelectionPlugin::convertSelection,
                               (QCoreApplication::translate("PythonDocMeshObjectSelection","Convert the selection on given object.Conversion must be given as strings.")+conversionStrings).toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","ID of the object").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","string of selection type which will be converted").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","string of selection type").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","unselect original selection after conversion").toLatin1().data()) );

  select.def("conversion", &MeshObjectSelectionPlugin::conversion,
                               (QCoreApplication::translate("PythonDocMeshObjectSelection","Convert selection on all target Objects. Conversion must be given as strings.")+conversionStrings).toLatin1().data(),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","string of selection type which will be converted").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","string of selection type").toLatin1().data()),
                        py::arg(QCoreApplication::translate("PythonDocMeshObjectSelection","unselect original selection after conversion").toLatin1().data()) );


}

